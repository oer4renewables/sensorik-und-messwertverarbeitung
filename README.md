# Unterlagen Sensorik und Messwertverarbeitung <img src="files/img/sensorik_logo.png" width=100 />

*Sensorik und Messwertverarbeitung* ist ein Kurs im Bachelor Elektrotechnik an der Technischen Hochschule Köln. Der Kurs vermittelt die physikalische Funktionsweise von Sensoren und die Auswirkungen der Messwertverarbeitung auf Messwerte. Dazu sind die Lehrinhalte in interaktive Jupyter Notebooks aufgeteilt. Die praktische Anwendung findet begleitend in der ULP statt. 

## Termine für den laufenden Kurs

In der [Übersicht](https://oer4renewables.gitlab.io/sensorik-und-messwertverarbeitung/files/01/SEN-orga.html#terminplan) findet sich der laufend aktualisierte Plan. 

Der Kurs findet bis auf weiteres in Präsenz statt. Für ausnahmsweise online-Termine finden Sie den Link in ILIAS (s.u.). 

## Anmelden

- **Prüfung an der TH Köln**: in [PSSO](https://psso.th-koeln.de)
- **Anmeldung für Termine**: in [ILIAS - F07 - Dozenten - May - SEN](https://ilu.th-koeln.de/goto.php?target=crs_49020&client_id=thkilu)
- **python online nutzen**: [jupyterhub](https://jupyterhub.th-koeln.de) erreichbar mit [VPN](https:/www.th-koeln.de/hochschule/vpn---virtual-private-network_26952.php), wer im Kurs in ILIAS ist wird dafür freigeschaltet. Eine kurze Nutzungsanleitung gibt es [hier](./files/jup/jupyterhub-intro.ipynb). Das Arbeiten mit dem jupyterhub bietet den Vorteil, dass keine eigene python-Installation nötig ist.

## Unterlagen

... werden während des Semesters aktualisiert:

- **[gitlab](https://gitlab.com/oer4renewables/sensorik-und-messwertverarbeitung)** Hier können Sie die jeweils aktuellste Version der Unterlagen herunterladen und die Notebooks (`.ipynb`) entweder auf Ihrem eigenen Rechner oder auf dem jupyterhub ausführen. 
- **[html-Buch](http://oer4renewables.gitlab.io/sensorik-und-messwertverarbeitung/README.html)** Dieses Buch beinhaltet in strukturierter Version alle Unterlagen. Sie können interaktive Diagramme verwenden oder Code einblenden, jedoch nicht ausführen. 
- **[pdf-Buch](http://oer4renewables.gitlab.io/ssensorik-und-messwertverarbeitung/sensorik.pdf)** Dieses Buch beinhaltet in strukturierter Version alle nicht-interaktiv darstellbaren Unterlagen.

## Zitieren

```
@misc{may_2023_987654321,
  author       = {May, Johanna Friederike},
  title        = {Unterlagen Sensorik und Messwertverarbeitung},
  month        = mar,
  year         = 2023,
  publisher    = {Zenodo},
  doi          = {xx.xxxx/zenodo.xxxxxxx},
  url          = {https://doi.org/xx.xxxx/zenodo.xxxxxxx}
}
```

## Danksagung

Die Materialien sind als open educational resources (OER) verfügbar unter der [Lizenz CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode). 

Besonderer Dank gilt meinen Studierenden, die durch ihre Fragen, Anregungen und Ideen zur kontinuierlichen Weiterentwicklung dieses Moduls und zu vielen interessanten fachlichen Diskussionen beitragen. 
